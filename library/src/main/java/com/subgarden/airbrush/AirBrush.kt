package com.subgarden.airbrush

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Point
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import androidx.annotation.WorkerThread
import androidx.palette.graphics.Palette
import com.subgarden.airbrush.loaders.GradientPalette
import kotlin.math.roundToInt

/**
 * @author Fredrik Larsen (fredrik@subgarden.com)
 */
class AirBrush(private val context: Context) {

    companion object {
        /** The reusable RenderScript instance */
        @SuppressLint("StaticFieldLeak")
        private var renderScript: RenderScript? = null

        private fun getRenderScript(context: Context): RenderScript {
            if (renderScript == null) {
                renderScript = RenderScript.create(context)
            }
            return renderScript!!
        }

        /** Releases any static references held by AirBrush. */
        fun cleanup() {
            renderScript = null
        }

        /**
         * Convenience method to get a palette from a bitmap. This code is not very efficient.
         */
        @WorkerThread
        fun getPalette(bitmap: Bitmap): GradientPalette {
            val divisions = 6
            val width = bitmap.width - 1
            val height = bitmap.height - 1

            val upperLeftPoint = Point(width / divisions, height / divisions)
            val upperRightPoint = Point(width - width / divisions, height / divisions)

            val lowerLeftPoint = Point(width / divisions, height - height / divisions)
            val lowerRightPoint = Point(width - width / divisions, height - height / divisions)

            val upperLeftColor = getDominantColorFromPoint(bitmap, upperLeftPoint)
            val upperRightPixel = getDominantColorFromPoint(bitmap, upperRightPoint)
            val lowerLeftPixel = getDominantColorFromPoint(bitmap, lowerLeftPoint)
            val lowerRightPixel = getDominantColorFromPoint(bitmap, lowerRightPoint)

            return GradientPalette(upperLeftColor, upperRightPixel, lowerRightPixel, lowerLeftPixel)
        }

        private fun getDominantColorFromPoint(bitmap: Bitmap, upperLeftPoint: Point): Int {
            val crop = Bitmap.createBitmap(bitmap, upperLeftPoint.x, upperLeftPoint.y, 25, 25)

            val color = Palette.from(crop).generate().getDominantColor(Color.GRAY)
            crop.recycle()
            return color
        }

        @WorkerThread
        fun blur(context: Context, image: Bitmap, scale: Float, radius: Float): Bitmap {
            val width = (image.width * scale).roundToInt()
            val height = (image.height * scale).roundToInt()

            val inputBitmap = Bitmap.createScaledBitmap(image, width, height, false)
            val outputBitmap = Bitmap.createBitmap(inputBitmap)

            val renderScript = getRenderScript(context)
            val blurScript = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript))
            val inAllocation = Allocation.createFromBitmap(renderScript, inputBitmap)
            val outAllocation = Allocation.createFromBitmap(renderScript, outputBitmap)

            blurScript.setRadius(radius)
            blurScript.setInput(inAllocation)
            blurScript.forEach(outAllocation)
            outAllocation.copyTo(outputBitmap)
            blurScript.destroy()

            inAllocation.destroy()
            outAllocation.destroy()

            return outputBitmap
        }

    }

    @WorkerThread
    fun getGradient(
        palette: GradientPalette,
        width: Int,
        height: Int
    ): Bitmap {
        // Gradients look unacceptable in RGB565
        val bitmapOut = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)

        val renderScript = getRenderScript(context)
        val inAllocation = Allocation.createFromBitmap(renderScript, bitmapOut)
        val outAllocation = Allocation.createFromBitmap(renderScript, bitmapOut)

        val paletteScript = ScriptC_palette(renderScript)

        paletteScript._gIn = inAllocation
        paletteScript._gOut = outAllocation
        paletteScript._gScript = paletteScript

        paletteScript._gTopLeftColor = palette.topLeft
        paletteScript._gTopRightColor = palette.topRight
        paletteScript._gBottomRightColor = palette.bottomRight
        paletteScript._gBottomLeftColor = palette.bottomLeft

        paletteScript._gWidthPixels = width
        paletteScript._gHeightPixels = height
        paletteScript.invoke_filter()

        outAllocation.copyTo(bitmapOut)

        inAllocation.destroy()
        outAllocation.destroy()

        paletteScript.destroy()
        return bitmapOut
    }

}
