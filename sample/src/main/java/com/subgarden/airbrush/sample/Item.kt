package com.subgarden.airbrush.sample

import androidx.annotation.DrawableRes
import com.subgarden.airbrush.loaders.GradientPalette
import com.subgarden.airbrush.loaders.TinyThumb

/**
 * @author Fredrik Larsen (fredrik@subgarden.com)
 */
sealed class Item(@DrawableRes open val resourceId: Int)

data class GradientPaletteItem(
    @DrawableRes override val resourceId: Int,
    val palette: GradientPalette
) : Item(resourceId)

data class TinyThumbItem(
    @DrawableRes override val resourceId: Int,
    val thumb: TinyThumb
) : Item(resourceId)
