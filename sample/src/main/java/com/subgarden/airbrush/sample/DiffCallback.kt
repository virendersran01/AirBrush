package com.subgarden.airbrush.sample

import androidx.recyclerview.widget.DiffUtil

class DiffCallback : DiffUtil.ItemCallback<Item>() {

    override fun areItemsTheSame(oldItem: Item, newItem: Item) =
        oldItem.resourceId == newItem.resourceId

    override fun areContentsTheSame(
        oldItem: Item,
        newItem: Item
    ) = oldItem == newItem

    override fun getChangePayload(
        oldItem: Item,
        newItem: Item
    ): Any = newItem

}